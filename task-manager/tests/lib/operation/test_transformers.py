import unittest

from lib.operation.transformers import ResourceTransformer


class TestResourceTransformer(unittest.TestCase):

    def setUp(self):
        self.input = {
            "test": {
                "a": 1,
                "b": 2
            },
            "test2": [
                {
                    "a": 1,
                    "b": 2
                }
            ]
        }
        self.output = {
            "test.a": 1,
            "test.b": 2,
            "test2": [{'a': 1, 'b': 2}]
        }

    def test_parser(self):

        self.assertEqual(
            ResourceTransformer(None)._parser(self.input, 0),
            self.output
        )


if __name__ == '__main__':
    unittest.main()
