"""
Task manager kernel
"""
import time
import argparse
import logging
import os

from lib.transport import DeserializeBodyException,\
                          SerializeBodyException,\
                          JSONTransport
from lib.broker import MessageBrokerReader,\
                       ConnectionBroker, RabbitBrokerConsumer,\
                       RabbitBrokerSender,\
                       RabbitFanoutChannel, RabbitTaskChannel,\
                       ConsumerHandler, ConsumerStrategyHandler
from lib.operation.mongo_connector import MongoConsumerStrategy
from lib.operation.strategy import StrategyFailureException
from lib.operation.transformers import BundleTransformer, ResourceTransformer
from lib.exception import ForceReconnectException


AVAILABLE_LEVELS = {
    "debug": logging.DEBUG,
    "info": logging.INFO,
    "warning": logging.WARNING,
    "error": logging.ERROR,
}

# AVAILABLE_OPERATIONS = ["pubsub", "fanout", "taskqueue"]

TRANFORMER_MAP = {
    "mongo_consumer": MongoConsumerStrategy,
    "bundle": BundleTransformer,
    "resource_flatten": ResourceTransformer
}

CHANNEL_TYPE = {
    "fanout": RabbitFanoutChannel,
    "taskqueue": RabbitTaskChannel
}


def main(input: MessageBrokerReader,
         handler: ConsumerHandler,
         logger=None
         ) -> None:

    try:
        if logger:
            logger.info("Base container starting")

        input.consume(handler)

    except SerializeBodyException as e:

        if logger:
            logger.info(f"transport serialize error: {e}")

        raise ForceReconnectException()

    except DeserializeBodyException as e:

        if logger:
            logger.info(f"transport deserialize error: {e}")

        raise ForceReconnectException()

    except StrategyFailureException as e:

        if logger:
            logger.info(f"StrategyFailureException error: {e}")

        raise ForceReconnectException()

    except Exception as e:

        raise ForceReconnectException(e)

    finally:
        input.stop()
        input.disconnect()


if __name__ == "__main__":
    try:

        parser = argparse.ArgumentParser(description='Task manager')

        parser.add_argument('--input', dest='input_broker_type',
                            type=str, nargs='?',
                            choices=CHANNEL_TYPE.keys(),
                            help=f'input channel: {CHANNEL_TYPE.keys()}')

        parser.add_argument('--output', dest='output_broker_type',
                            type=str, nargs='?',
                            choices=CHANNEL_TYPE.keys(),
                            help=f'output channel: {CHANNEL_TYPE.keys()}')

        parser.add_argument('--transformer', dest='transformer',
                            type=str, nargs='?',
                            choices=TRANFORMER_MAP.keys(),
                            help=f'transformers: {TRANFORMER_MAP.keys()}')

        args = parser.parse_args()

        logger = logging.getLogger(os.environ["LOGGER_NAME"])
        logger.setLevel(AVAILABLE_LEVELS[os.environ["LOGGING_LEVEL"]])
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s")

        fh = logging.FileHandler(
            "logs/{}.log".format(os.environ["LOGGER_NAME"]))
        fh.setLevel(AVAILABLE_LEVELS[os.environ["LOGGING_LEVEL"]])
        fh.setFormatter(formatter)
        logger.addHandler(fh)

        # logging to console
        ch = logging.StreamHandler()
        ch.setLevel(AVAILABLE_LEVELS[os.environ["LOGGING_LEVEL"]])
        ch.setFormatter(formatter)
        logger.addHandler(ch)

        connection_broker = ConnectionBroker(os.environ["RMQ_USER"],
                                             os.environ["RMQ_PASSWORD"],
                                             os.environ["RMQ_HOST"],
                                             os.environ["RMQ_PORT"],
                                             logger)

        channel_input = None
        channel_output = None

        if args.input_broker_type:

            channel_input = CHANNEL_TYPE[args.input_broker_type](
                os.environ["EXCHANGE_INPUT"],
                os.environ["EXCHANGE_INPUT_ROUTING_KEY"],
                logger
            )

        consumer = RabbitBrokerConsumer(
            connection_broker,
            channel_input,
            logger
        )

        if args.output_broker_type:

            channel_output = CHANNEL_TYPE[args.output_broker_type](
                os.environ["EXCHANGE_OUTPUT"],
                os.environ["EXCHANGE_OUTPUT_ROUTING_KEY"],
                logger
            )

            sender = RabbitBrokerSender(
                connection_broker,
                channel_output,
                logger
            )
        else:
            sender = None

        operation = TRANFORMER_MAP[args.transformer](logger)

        if isinstance(operation, MongoConsumerStrategy):
            operation.init(
                connection_string='mongodb://{}:{}@{}:{}/'.format(
                    os.environ["MONGO_USERNAME"],
                    os.environ["MONGO_PASSWORD"],
                    os.environ["MONGO_HOST"],
                    os.environ["MONGO_PORT"]
                ),
                database=os.environ["MONGO_DATABASE"]
            )

        handler = ConsumerStrategyHandler(
            operation=operation,
            transport=JSONTransport(),
            output=sender,
            logger=logger
        )

        main(consumer,
             handler,
             logger,
             os.environ["SOFTWARE_REBOOT_AFTER"]
             )

    except ForceReconnectException:

        if logger:
            logger.info("rebooting:")
        time.sleep(int(os.environ["SOFTWARE_REBOOT_AFTER"]))

    except KeyboardInterrupt:
        pass
