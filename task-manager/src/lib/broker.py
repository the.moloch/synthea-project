
import abc
from typing import Tuple, Any, Optional
import pika
from lib.operation.strategy import Strategy, StrategyFailureException
from lib.transport import Transport,\
                            SerializeBodyException,\
                            DeserializeBodyException


class MessageBroker(abc.ABC):

    @abc.abstractmethod
    def connect(self) -> Any:
        raise NotImplementedError

    @abc.abstractmethod
    def disconnect(self) -> None:
        raise NotImplementedError


class MessageBrokerChannel(abc.ABC):

    @abc.abstractmethod
    def declare_channel(self) -> Tuple:
        raise NotImplementedError

    @abc.abstractmethod
    def transport_properties(self) -> Any:
        raise NotImplementedError

    @abc.abstractmethod
    def disconnect(self) -> None:
        raise NotImplementedError


class MessageBrokerSender(abc.ABC):
    @abc.abstractmethod
    def send(self) -> None:
        raise NotImplementedError


class MessageBrokerReader(abc.ABC):
    @abc.abstractmethod
    def consume(self, consumer) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def stop(self) -> None:
        raise NotImplementedError


class ConsumerHandler(abc.ABC):
    @abc.abstractmethod
    def consumer_handler(self) -> Any:
        raise NotImplementedError


class ConsumerStrategyHandler(ConsumerHandler):

    def __init__(self,
                 operation: Strategy,
                 transport: Transport,
                 output: Optional[MessageBrokerSender],
                 logger=None):
        self.logger = logger

        self.operation = operation
        self.transport = transport
        self.output = output

    def consumer_handler(self) -> Any:

        def process_message(ch, method, properties, body):

            try:

                body_raw = self.transport.deserialize(body)
                # generator go ahead, one element per time
                for result in self.operation.consume(method,
                                                     properties,
                                                     body_raw):
                    ready_to_push = self.transport.serialize(result)
                    if self.output:
                        # i have to push to output
                        if self.logger:
                            self.logger.debug("message sent to fanout")
                        self.output.send(ready_to_push)

                ch.basic_ack(delivery_tag=method.delivery_tag)

            except ValueError as e:
                if self.logger:
                    self.logger.error(e)
            except DeserializeBodyException as e:
                if self.logger:
                    self.logger.error(e)
            except SerializeBodyException as e:
                if self.logger:
                    self.logger.error(e)
            except StrategyFailureException as e:
                if self.logger:
                    self.logger.error(e)

        return process_message


class ConnectionBroker(MessageBroker):

    def __init__(self,  user,
                 password,
                 host,
                 port,
                 logger
                 ) -> None:

        self.user = user
        self.password = password

        self.host = host
        self.port = port

        self.connection = None

        self.logger = logger

    def connect(self):

        if not self.connection:

            credentials = pika.PlainCredentials(self.user,
                                                self.password)
            self.connection = pika.BlockingConnection(
                pika.ConnectionParameters(host=self.host,
                                          port=self.port,
                                          credentials=credentials))
            if self.logger:
                self.logger.info(
                    f"Connection with rabbitmq established {self.host}: {self.port}")

        return self.connection

    def disconnect(self) -> None:

        if self.connection:

            self.connection.close()
            self.connection = None


class RabbitFanoutChannel(MessageBrokerChannel):

    def __init__(self,
                 exchange: str,
                 routing_key: str,
                 logger) -> None:

        self.channel = None
        self.queue_name = None

        self.exchange = exchange
        self.routing_key = routing_key

        self.logger = logger

    def declare_channel(self, connection: ConnectionBroker) -> Tuple:

        if not self.channel:

            channel = connection.connect().channel()
            channel.exchange_declare(
                exchange=self.exchange, exchange_type="fanout")

            if self.logger:
                self.logger.info(f"channel connected, {self.exchange} fanout")
            # get the queue
            declared_queue = channel.queue_declare(queue="", exclusive=True)
            queue_name = declared_queue.method.queue
            channel.queue_bind(exchange=self.exchange,
                               queue=queue_name,
                               routing_key=self.routing_key)
            if self.logger:
                self.logger.info(f"queue declared and bound, {queue_name}")

            self.channel = channel
            self.queue_name = queue_name

        return self.channel, self.queue_name

    def transport_properties(self) -> pika.BasicProperties:
        return None

    def disconnect(self) -> None:
        if self.channel:
            self.channel.close()
            self.channel = None


class RabbitTaskChannel(MessageBrokerChannel):

    def __init__(self,
                 exchange: str,
                 routing_key: str,
                 logger) -> None:

        self.channel = None
        self.queue_name = None

        self.exchange = exchange
        self.routing_key = routing_key

        self.logger = logger

    def declare_channel(self, connection: ConnectionBroker) -> Tuple:

        if not self.channel:

            channel = connection.connect().channel()
            channel.exchange_declare(
                exchange=self.exchange, exchange_type="direct")

            if self.logger:
                self.logger.info(f"channel connected, {self.exchange} direct")
            # name the queue make durable
            declared_queue = channel.queue_declare(queue=self.routing_key,
                                                   durable=True)
            channel.basic_qos(prefetch_count=1)
            # get the queue
            declared_queue = channel.queue_declare(queue="", exclusive=True)
            queue_name = declared_queue.method.queue
            channel.queue_bind(exchange=self.exchange,
                               queue=queue_name,
                               routing_key=self.routing_key)
            if self.logger:
                self.logger.info(f"queue declared and bound, {queue_name}")

            self.channel = channel
            self.queue_name = queue_name

        return self.channel, self.queue_name

    def disconnect(self) -> None:
        if self.channel:
            self.channel.close()
            self.channel = None

    def transport_properties(self) -> pika.BasicProperties:
        return pika.BasicProperties(
                        delivery_mode=2,  # make message persistent
                    )


class RabbitBrokerSender(MessageBrokerSender):

    def __init__(self,
                 connection: ConnectionBroker,
                 channel_factory: MessageBrokerChannel,
                 logger=None
                 ) -> None:

        self.connection = connection

        self.channel_factory = channel_factory

        ch_connected, q_name = channel_factory.declare_channel(connection)

        self.channel_connected = ch_connected
        self.queue_name = q_name

        self.logger = logger

    def send(self, message) -> None:

        self.channel_connected.basic_publish(
                exchange=self.channel_factory.exchange,
                routing_key=self.channel_factory.routing_key,
                body=message,
                properties=self.channel_factory.transport_properties())

    def disconnect(self) -> None:
        self.channel_factory.disconnect()
        self.connection.disconnect()


class RabbitBrokerConsumer(MessageBrokerReader):

    def __init__(self,
                 connection: ConnectionBroker,
                 channel_factory: MessageBrokerChannel,
                 logger
                 ) -> None:

        self.connection = connection

        self.channel_factory = channel_factory

        ch_connected, q_name = channel_factory.declare_channel(connection)

        self.channel_connected = ch_connected
        self.queue_name = q_name

        self.logger = logger

    def consume(self, consumer: ConsumerHandler) -> None:

        self.channel_connected.basic_consume(
            queue=self.queue_name,
            on_message_callback=consumer.consumer_handler())

        self.channel_connected.start_consuming()

    def stop(self):
        self.channel_connected.stop_consuming()

    def disconnect(self) -> None:
        self.channel_factory.disconnect()
        self.connection.disconnect()
