"""
this is a mongo consumer, 
takes the input and push in db
I get the variables from the environment
"""

from pymongo import MongoClient

from lib.operation.strategy import Strategy


class MongoConsumerStrategy(Strategy):
    """
    load at runtime will override the Strategy in the container
    """

    def __init__(self, logger):
        self.logger = logger

    def init(self, connection_string: str, database: str) -> None:
        self.client = MongoClient(connection_string)
        self.database = self.client[database]

    def consume(self, method, properties, body):
        """
        at this level i have in input a json object,
        i need to push in the right collection, which is
        mapped by resourceType
        so that I have one collection per resourceType
        """

        self.database[body["resourceType"]].update_one(
            {"_id": body["id"]},
            {'$set': body},
            upsert=True
        )
        # no output
        yield None
