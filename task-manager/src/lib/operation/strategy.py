
import abc


class StrategyFailureException(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class Strategy(abc.ABC):
    """
    stub
    """

    def __init__(self, logger):
        self.logger = logger

    @abc.abstractmethod
    def consume(self, method, properties, body):
        raise NotImplementedError
