"""
operations on the resource
- extractor
- mappers

input: Resource
output: Flattened object
"""
from lib.operation.strategy import Strategy


class ResourceTransformer(Strategy):
    """
    Transform and flattens a resource
    """

    def __init__(self, logger):
        self.logger = logger

    @classmethod
    def _parser(cls, t, n, base=None, result=None):
        """
        recurrent parsing of object, it flattens the object in input
        """

        if base is None:
            base = []
        if result is None:
            result = {}

        if isinstance(t, dict):

            for q in t:

                result = cls._parser(t[q], n + 1, base + [q], result)

            return result

        elif isinstance(t, list):

            result[".".join(base)] = []
            for _, el in enumerate(t):
                result[".".join(base)].append(cls._parser(el, n + 1, None, None))

            return result

        # value, leaf of the tree
        result[".".join(base).replace(".[", "[")] = t

        return result

    def consume(self, method, properties, body):
        """
        at this level i have in input a json object,

        i need to flatten the object, nested arrays are discarded
        it can be modified later in the pipeline
        """

        yield self._parser(body, 0)


class BundleTransformer(Strategy):
    """
    extract resources from a bundle
    """

    def __init__(self, logger):
        self.logger = logger

    def consume(self, method, properties, body):
        """
        at this level i have in input a json object, I need to split the entries
        """
        if "entry" in body:
            for element in body["entry"]:
                if "resource" in element:
                    self.logger.debug(element["resource"])
                    yield element["resource"]
        else:
            raise ValueError
