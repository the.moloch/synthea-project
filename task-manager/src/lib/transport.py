import json
import abc


class DeserializeBodyException(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class SerializeBodyException(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class Transport(abc.ABC):

    @abc.abstractmethod
    def deserialize(self, message):
        raise NotImplementedError

    @abc.abstractmethod
    def serialize(self, message):
        raise NotImplementedError


class JSONTransport(Transport):

    def deserialize(self, message):
        return json.loads(message.decode("utf-8"))

    def serialize(self, message):
        return json.dumps(message)
