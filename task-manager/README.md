Task manager

A drafted kernel to build a pipeline built on top of RabbitMQ.


# input/output type:
- fanout
- taskqueue


# variables (No defaults):

LOGGER_NAME: name of the file

LOGGING_LEVEL: debug, info, warning, error


RMQ_USER: rabbitmq user

RMQ_PASSWORD: rabbitmq password

RMQ_HOST: rabbitmq host

RMQ_PORT: rabbitmq port

EXCHANGE_INPUT: 

EXCHANGE_INPUT_ROUTING_KEY: 

EXCHANGE_OUTPUT: 

EXCHANGE_OUTPUT_ROUTING_KEY: 

SOFTWARE_REBOOT_AFTER: number of seconds


# Entry point (docker compose):

command example:
["python3", "main.py","--input","fanout", "--output","taskqueue","--transformer","bundle"]

# Transformers:
- bundle
- resource_flatten
- mongo_consumer

