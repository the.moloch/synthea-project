
import abc
import pika


class MessageBroker(abc.ABC):

    @abc.abstractmethod
    def disconnect(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def send(self) -> None:
        raise NotImplementedError


class RabbitFanoutBroker(MessageBroker):

    def __init__(self,  user,
                 password,
                 host,
                 port,
                 exchange,
                 routing_key,
                 logger
                 ) -> None:

        self.user = user
        self.password = password

        self.host = host
        self.port = port

        self.exchange = exchange
        self.routing_key = routing_key

        self.logger = logger

        self.connection = None
        self.channel = None

    def _connect(self):

        if not self.connection:

            credentials = pika.PlainCredentials(self.user,
                                                self.password)
            self.connection = pika.BlockingConnection(
                pika.ConnectionParameters(host=self.host,
                                          port=self.port,
                                          credentials=credentials))
            if self.logger:
                self.logger.info(
                    f"Connection with rabbitmq established {self.host}: {self.port}")

        return self.connection

    def disconnect(self) -> None:

        if self.connection:

            if self.channel:
                self.channel.close()
                self.channel = None

            self.connection.close()
            self.connection = None

    def _declare_channel(self):

        if not self.channel:

            channel = self._connect().channel()
            channel.exchange_declare(
                exchange=self.exchange, exchange_type="fanout")

            if self.logger:
                self.logger.info(f"channel connected, {self.exchange} fanout")
            # get the queue
            declared_queue = channel.queue_declare(queue="", exclusive=True)
            queue_name = declared_queue.method.queue
            channel.queue_bind(exchange=self.exchange,
                               queue=queue_name,
                               routing_key=self.routing_key)
            if self.logger:
                self.logger.info(f"queue declared and bound, {queue_name}")

            self.channel = channel

        return self.channel

    def send(self, message) -> None:

        self._declare_channel().basic_publish(exchange=self.exchange,
                                              routing_key=self.routing_key,
                                              body=message)
