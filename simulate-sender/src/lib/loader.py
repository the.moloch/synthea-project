

import glob
import abc


class DataLoader(abc.ABC):

    @abc.abstractmethod
    def next_message(self) -> str:
        raise NotImplementedError


class FileDataLoader(DataLoader):

    def __init__(self, directory, logger=None) -> None:

        self.directory = directory

        self.logger = logger

    def _next_file(self):

        files = glob.glob(self.directory)

        if self.logger:
            self.logger.debug(files)
        # read and push
        for file in files:
            yield file

    def next_message(self):

        for file in self._next_file():

            if self.logger:
                self.logger.debug(f"pushing {file}")

            with open(file, "rb") as fp:
                message = fp.read()
                yield message
