
import time
import logging
import os
from lib.broker import MessageBroker, RabbitFanoutBroker
from lib.loader import DataLoader, FileDataLoader

AVAILABLE_LEVELS = {
    "debug": logging.DEBUG,
    "info": logging.INFO,
    "warning": logging.WARNING,
    "error": logging.ERROR,
}


class ForceReconnectException(Exception):
    pass


def main(broker: MessageBroker,
         scanner: DataLoader,
         logger=None,
         reboot_after=1
         ) -> None:

    try:
        if logger:
            logger.info("Base container starting")

        for message in scanner.next_message():
            broker.send(message)

    except Exception as e:
        if logger:
            logger.info("rebooting : {}".format(e))
        time.sleep(int(reboot_after))
        raise ForceReconnectException()
    finally:
        if broker:
            broker.disconnect()


if __name__ == "__main__":
    try:

        logger = logging.getLogger(os.environ["LOGGER_NAME"])
        logger.setLevel(AVAILABLE_LEVELS[os.environ["LOGGING_LEVEL"]])
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s")

        fh = logging.FileHandler(
            "logs/{}.log".format(os.environ["LOGGER_NAME"]))
        fh.setLevel(AVAILABLE_LEVELS[os.environ["LOGGING_LEVEL"]])
        fh.setFormatter(formatter)
        logger.addHandler(fh)

        # logging to console
        ch = logging.StreamHandler()
        ch.setLevel(AVAILABLE_LEVELS[os.environ["LOGGING_LEVEL"]])
        ch.setFormatter(formatter)
        logger.addHandler(ch)

        broker = RabbitFanoutBroker(os.environ["RMQ_USER"],
                                    os.environ["RMQ_PASSWORD"],
                                    os.environ["RMQ_HOST"],
                                    os.environ["RMQ_PORT"],
                                    os.environ["EXCHANGE_OUTPUT"],
                                    os.environ["EXCHANGE_OUTPUT_ROUTING_KEY"],
                                    logger)

        scanner = FileDataLoader(os.environ["DATA_DIRECTORY"])

        main(broker,
             scanner,
             logger,
             os.environ["SOFTWARE_REBOOT_AFTER"]
             )

    except KeyboardInterrupt:
        pass
