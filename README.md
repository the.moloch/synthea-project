# synthea-project

# for draft
python3 -m venv env
. env/bin/activate
python3 -m pip install pip --upgrade
python3 -m pip install setuptools --upgrade
python3 -m pip install -r requirements.txt



# for docker-compose

cat >> .env <<EOF
SOFTWARE_REBOOT_AFTER=5
LOGGING_LEVEL=debug
RMQ_USER=user
RMQ_PASSWORD=password
RMQ_HOST=rabbitmq
RMQ_PORT=5672
MONGO_HOST=mongo
MONGO_PORT=27017
MONGO_INITDB_ROOT_USERNAME=mongo
MONGO_INITDB_ROOT_PASSWORD=example
MONGO_DATABASE=synthea
ME_CONFIG_MONGODB_ADMINUSERNAME=mongo
ME_CONFIG_MONGODB_ADMINPASSWORD=example
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_DB=synthea
SYSTEM_ENTRY_POINT=entrypoint
SYSTEM_ENTRY_POINT_ROUTING_KEY=entrypoint
DATA_DIRECTORY=data/*.json
ENTRY_POINT_EXCHANGE_OUTPUT=route_to_mongo
ENTRY_POINT_EXCHANGE_OUTPUT_ROUTING_KEY=route_to_mongo
RESOURCE_MAPPED_EXCHANGE=resource_mapped
RESOURCE_MAPPED_TASK_QUEUE=resource_mapped
EOF

export $(grep -v '^#' .env | xargs)


docker-compose up --build