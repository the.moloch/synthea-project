import json
import os
import glob
import pandas as pd
from collections import defaultdict

directory = "../../exa-data-eng-assessment/data/*.json"
test_value = "Sandee884_Hodkiewicz467_15201e77-7e36-0667-73bc-013783e57649.json"

fileList = glob.glob(directory)


def parser(t, n, base=None, result=None):
    '''
    recurrent parsing of object, it flattens the object in input
    '''
    if base is None:
        base = []
    if result is None:
        result = {}

    if isinstance(t, dict):
        # print(" "*n,t.keys())
        # print("\nD:"," "*n,t.keys())
        for q in t:
            # print(" "*n,q)
            result = parser(t[q], n + 1, base + [q], result)
        return result
    elif isinstance(t, list):
        # print("\n",t)
        # print(".".join(base),"-L:",t)
        # if base[-1] == "coding":
        result[".".join(base)] = []
        for index, el in enumerate(t):
            result[".".join(base)].append(parser(el, n + 1, None, None))
        return result
    # value, leaf of the tree
    result[".".join(base).replace(".[", "[")] = t
    return result


reference = defaultdict(set)
for file in fileList:
    print(file)
    with open(file, "r") as fp:
        prova = json.load(fp)
    # bundle=Bundle.parse_obj(prova)

    # print(bundle.__dict__)
    # exit()

    for element in prova["entry"]:
        target = parser(element["resource"], 0)
        # for key in target:
        #    if isinstance(target[key],list):
        #        reference[target[key]].update(target[key].keys())
        reference[target["resourceType"]].update(target.keys())
        # print(pd.DataFrame.from_dict(target,orient="index"))
        print(target)
        exit()
print(reference)
exit()
