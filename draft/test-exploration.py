import json
import os
import glob
import pandas as pd

directory = "../../exa-data-eng-assessment/data/*.json"
test_value = "Sandee884_Hodkiewicz467_15201e77-7e36-0667-73bc-013783e57649.json"

fileList = glob.glob(directory)


def parser(t, n, base=None):
    '''
    recurrent parsing of object, it flattens the object in input
    '''
    if base is None:
        base = []

    if isinstance(t, dict):
        # print(" "*n,t.keys())
        # print("\nD:"," "*n,t.keys())
        for q in t:
            # print(" "*n,q)

            parser(t[q], n + 1, base + [q])
        return
    elif isinstance(t, list):
        # print("\n",t)
        # print(".".join(base),"-L:",t)
        for index, el in enumerate(t):
            parser(el, n + 1, base + [f"[{index}]"])
        return
    # value, leaf of the tree
    print(".".join(base).replace(".[", "["), ":", t)


for file in fileList:
    print(file)
    with open(file, "r") as fp:
        prova = json.load(fp)
    # bundle=Bundle.parse_obj(prova)

    # print(bundle.__dict__)
    # exit()
    parser(prova["entry"][234]["resource"], 0)

    exit()
